﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace RuntimeGeneration
{
    class Program
    {
        delegate int MultiplyDelegate( int a, int b );

        static int multiply( int a, int b ) => a * b;

        static void Main( string[] args )
        {
            ilTimer();

            methodWithConsole();

            expressionsTimer();
        }

        private static void expressionsTimer()
        {
            Expression<Func<int, int>> expression = x => x * 5;

            Func<int, int> compiled = expression.Compile();

            Stopwatch sw = Stopwatch.StartNew();
            for ( int i = 0; i < 1000000; i++ )
            {
                int res = compiled( 5 );
            }
            sw.Stop();
            Console.WriteLine( $"compiled expression {sw.ElapsedMilliseconds}ms" );
            Console.WriteLine( $"res: {compiled( 5 )}" );


            Expression                 right  = Expression.Constant( 5 );
            ParameterExpression        left   = Expression.Parameter( typeof( int ) );
            Expression<Func<int, int>> result = Expression.Lambda<Func<int, int>>(
                Expression.Multiply( left, right )
              , left
            );

            Func<int, int> compiled2 = result.Compile();

            sw = Stopwatch.StartNew();
            for ( int i = 0; i < 1000000; i++ )
            {
                int res = compiled2( 5 );
            }
            sw.Stop();
            Console.WriteLine( $"compiled expression {sw.ElapsedMilliseconds}ms" );
            Console.WriteLine( $"res: {compiled2( 5 )}" );
        }

        private static void ilTimer()
        {
            DynamicMethod mt = new DynamicMethod( "multiply", typeof( int ), new[] { typeof( int ), typeof( int ) } );

            ILGenerator il = mt.GetILGenerator();
            il.Emit( OpCodes.Ldarg_0 );
            il.Emit( OpCodes.Ldarg_1 );
            il.Emit( OpCodes.Mul );
            il.Emit( OpCodes.Ret );

            MultiplyDelegate delegete = (MultiplyDelegate) mt.CreateDelegate( typeof( MultiplyDelegate ) );

            object[] parameters = new object[] { 20, 3 };

            Stopwatch sw = Stopwatch.StartNew();
            for ( int i = 0; i < 1000000; i++ )
            {
                int res = (int) mt.Invoke( null, parameters );
            }

            sw.Stop();
            Console.WriteLine( $"il took {sw.ElapsedMilliseconds}ms" );

            sw = Stopwatch.StartNew();
            for ( int i = 0; i < 1000000; i++ )
            {
                int res = delegete( 20, 3 );
            }

            sw.Stop();
            Console.WriteLine( $"il compiled took {sw.ElapsedMilliseconds}ms" );

            sw = Stopwatch.StartNew();
            for ( int i = 0; i < 1000000; i++ )
            {
                int res = multiply( 20, 3 );
            }

            sw.Stop();
            Console.WriteLine( $"simple {sw.ElapsedMilliseconds}ms" );
        }

        static void methodWithConsole()
        {
            object[] parameters = new object[] { 20, 3 };

            MethodInfo method_info = typeof( Console ).GetMethod( nameof( Console.WriteLine ), new[] { typeof( int ) } );

            DynamicMethod mt = new DynamicMethod( "multiply", typeof( int ), new[] { typeof( int ), typeof( int ) } );

            ILGenerator il = mt.GetILGenerator();

            LocalBuilder a = il.DeclareLocal(typeof(Int32));
            il.Emit( OpCodes.Ldarg_0 );
            il.Emit( OpCodes.Ldarg_1 );
            il.Emit( OpCodes.Mul );
            il.Emit( OpCodes.Stloc, a );
            il.Emit( OpCodes.Ldloc, a );
            il.EmitCall( OpCodes.Call, method_info, null );
            il.Emit( OpCodes.Ldloc, a );
            il.Emit( OpCodes.Ret );

            int res = (int) mt.Invoke( null, parameters );
            Console.WriteLine( $"and result: {res}" );
        }
    }
}