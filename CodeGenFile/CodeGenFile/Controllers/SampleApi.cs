﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CodeGenFile.Models;

namespace CodeGenFile.Controllers
{
    [ApiController]
    [Route( "api/sample" )]
    public class SampleApi : Controller
    {

        [HttpGet( "sample" )]
        public IActionResult sampleRequest( int arg, string arg2 )
        {
            return Json( new { arg = arg, arg2 = arg2 } );
        }

        [HttpGet( "sample2" )]
        public IActionResult sampleRequest( MyClass arg_my_class )
        {
            return Json( arg_my_class );
        }
    }

    public class MyClass
    {
        public string val1 { get; set; }
        public int    val2 { get; set; }
    }
}