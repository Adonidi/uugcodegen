using CommandLine;

namespace GameService
{
  [Verb( "nswag", HelpText = "Generates Server API csharp client" )]
  public class SwaggerCliOptions
  {
    [Option( shortName: 'i', longName: "input", Required = true, HelpText = "Input file path" )]
    public string inputFilePath { get; set; }

    [Option( shortName: 'o', longName: "output", Required = true, HelpText = "Output file path" )]
    public string outputFilePath { get; set; }
  }
}