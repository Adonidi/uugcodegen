using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CommandLine;
using GameService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NSwag;
using NSwag.CodeGeneration.CSharp;

namespace CodeGenFile
{
    public class Program
    {
        public static async Task Main( string[] args )
        {
            ParserResult<object> result = await new Parser().ParseArguments<SwaggerCliOptions, object>( args )
                .WithParsedAsync(
                    async ( SwaggerCliOptions options ) => { await generateOpenApiCsharpClientAsync( options.inputFilePath, options.outputFilePath ); }
                );

            if ( result.Tag == ParserResultType.Parsed )
            {
                return;
            }

            CreateHostBuilder( args ).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder( string[] args ) =>
            Host.CreateDefaultBuilder( args )
                .ConfigureWebHostDefaults(
                    webBuilder =>
                    {
                        webBuilder.UseUrls( "https://localhost:5020;http://localhost:5021" );
                        webBuilder.UseStartup<Startup>();
                    }
                );

        private static async Task generateOpenApiCsharpClientAsync( string input_file_path, string output_file_path )
        {
            OpenApiDocument document = await OpenApiDocument.FromFileAsync( input_file_path );
            CSharpClientGeneratorSettings settings = new CSharpClientGeneratorSettings
            {
                CSharpGeneratorSettings = { Namespace = "Sample.App.Api" }
            };

            CSharpClientGenerator generator = new CSharpClientGenerator( document, settings );

            string code = generator.GenerateFile();

            await File.WriteAllTextAsync( output_file_path, code );
        }
    }
}