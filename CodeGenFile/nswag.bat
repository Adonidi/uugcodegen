IF not [%1]==[] (SET P=%1) ELSE (SET P=.)

SET API_VERSION="v1"
SET SWAG_FILE_PATH="%P%\CodeGenFile\bin\Debug\net5.0\CodeGenFile.swagger.json"
SET DLL_FILE_PATH="%P%\CodeGenFile\bin\Debug\net5.0\CodeGenFile.dll"
SET TARGET_CS_FILE_PATH="%P%\Api\ApiV1GameService.cs"

dotnet tool restore
dotnet swagger tofile --output %SWAG_FILE_PATH% %DLL_FILE_PATH% %API_VERSION%
dotnet %DLL_FILE_PATH% nswag -i %SWAG_FILE_PATH% -o %TARGET_CS_FILE_PATH%

pause