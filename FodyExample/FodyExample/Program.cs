﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FodyExample
{
    public class MyClass : INotifyPropertyChanged
    {
        public string prop1 { get; set; }
        public int    prop2 { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName, object before, object after)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs( propertyName ));
            Console.WriteLine( $"{propertyName} was {before} and now it is {after}" );
        }
    }

    class Program
    {
        static void Main( string[] args )
        {
            MyClass a = new MyClass();
            a.prop1 = "some text";
            a.prop2 = 6;

            a.prop1 = "some text2";
            a.prop2 = 9;

            a.prop1 = "some text3";
            a.prop2 = 1;
        }
    }
}